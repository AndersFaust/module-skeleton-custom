<?php


/**
 * Class ModuleSkeleton_Backoffice_ViewController
 */
class ModuleSkeleton_Backoffice_ViewController extends Backoffice_Controller_Default
{
    /**
     *
     */
    public function loadAction()
    {
        $payload = [
            "title" => sprintf("%s > %s > %s",
                __("Manage"), __("Modules"), p__("skeleton", "Skeleton")),
            "icon" => "fa fa-key",
            "settings" => [
                "skeleton_test" => __get("skeleton_test"),
            ]
        ];

        $this->_sendJson($payload);
    }

    /**
     *
     */
    public function saveAction()
    {
        try {
            $request = $this->getRequest();
            $settings = $request->getBodyParams();

            __set("skeleton_test", $settings["skeleton_test"]);

            $payload = [
                "success" => true,
                "message" => p__("cabride", "Settings saved.")
            ];

        } catch (\Exception $e) {
            $payload = [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }

        $this->_sendJson($payload);
    }
}
