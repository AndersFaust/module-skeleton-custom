<?php

/**
 * Class ModuleSkeleton_ApplicationController
 */
class ModuleSkeleton_ApplicationController extends Application_Controller_Default
{
    /**
     * Edit the default skeleton settings
     */
    public function saveAction()
    {
        try {
            $request = $this->getRequest();
            $params = $request->getPost();

            $form = new ModuleSkeleton_Form_Skeleton();

            if ($form->isValid($params)) {
                
                // Do whatever you need when form is valid!
                $optionValue = $this->getCurrentOptionValue();
                $valueId = $optionValue->getId();

                $skeleton = (new ModuleSkeleton_Model_Skeleton())
                    ->find($valueId, "value_id");

                $skeleton->setData($params);

                \Siberian\Feature::formImageForOption($optionValue, $skeleton, $params, "image", true);

                $skeleton->save();

                /** Update touch date, then never expires (until next touch) */
                $this->getCurrentOptionValue()
                    ->touch()
                    ->expires(-1);

                $payload = [
                    "success" => true,
                    "message" => __("Success."),
                ];
            } else {
                $payload = [
                    "error" => true,
                    "message" => $form->getTextErrors(),
                    "errors" => $form->getTextErrors(true)
                ];
            }
        } catch (Exception $e) {
            $payload = [
                "error" => true,
                "message" => $e->getMessage(),
            ];
        }

        $this->_sendJson($payload);
    }

    /**
     * Edit the default skeleton settings
     */
    public function saveItemAction() // create OR update controllers**
    {
        try {
            $request = $this->getRequest();
            $params = $request->getPost();

            $form = new ModuleSkeleton_Form_Item();

            if ($form->isValid($params)) {

                $optionValue = $this->getCurrentOptionValue();

                $item = (new ModuleSkeleton_Model_Item())
                    ->find($params["item_id"]);

                $item->setData($params);

                \Siberian\Feature::formImageForOption($optionValue, $item, $params, "cover", true);

                $item->save();

                /** Update touch date, then never expires (until next touch) */
                $this->getCurrentOptionValue()
                    ->touch()
                    ->expires(-1);

                $payload = [
                    "success" => true,
                    "message" => __("Success."),
                ];
            } else {
                $payload = [
                    "error" => true,
                    "message" => $form->getTextErrors(),
                    "errors" => $form->getTextErrors(true)
                ];
            }
        } catch (Exception $e) {
            $payload = [
                "error" => true,
                "message" => $e->getMessage(),
            ];
        }

        $this->_sendJson($payload);
    }

    /**
     *
     */
    public function loadItemFormAction() {
        try {
            $request = $this->getRequest();

            $itemId = $request->getParam("item_id");
            $valueId = $this->getCurrentOptionValue()->getId();

            $item = (new ModuleSkeleton_Model_Item())
                ->find($itemId);

            if (!$item->getId()) {
                throw new Exception(__("The item you are trying to edit doesn't exists."));
            }

            $form = new ModuleSkeleton_Form_Item();
            $form->populate($item->getData());
            $form->setValueId($valueId);

            $payload = [
                "success" => true,
                "form" => $form->render(),
                "message" => __("Success."),
            ];

        } catch (\Exception $e) {
            $payload = [
                "error" => true,
                "message" => $e->getMessage(),
            ];
        }

        $this->_sendJson($payload);
    }

    /**
     * Delete company
     */
    public function itemDeleteAction()
    {
        try {
            $request = $this->getRequest();
            $params = $request->getPost();

            $form = new ModuleSkeleton_Form_Item_Delete();
            if ($form->isValid($params)) {

                $item = (new ModuleSkeleton_Model_Item())
                    ->find($params["item_id"]);

                $item->delete();

                // Update touch date, then never expires (until next touch)!
                $this->getCurrentOptionValue()
                    ->touch()
                    ->expires(-1);

                $payload = [
                    "success" => true,
                    "message" => __("Item successfully deleted."),
                ];
            } else {
                $payload = [
                    "error" => true,
                    "message" => $form->getTextErrors(),
                    "errors" => $form->getTextErrors(true),
                ];
            }
        } catch (\Exception $e) {
            $payload = [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }

        $this->_sendJson($payload);
    }
}
