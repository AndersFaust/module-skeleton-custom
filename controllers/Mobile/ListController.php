<?php

/**
 * Class ModuleSkeleton_Mobile_ListController
 */
class ModuleSkeleton_Mobile_ListController extends Application_Controller_Mobile_Default
{
    /**
     *
     */
    public function loadContentAction()
    {
        try {
            $optionValue = $this->getCurrentOptionValue();
            $valueId = $optionValue->getId();

            $skeleton = (new ModuleSkeleton_Model_Skeleton())->find($valueId, "value_id");

            //$fakeCollection = [];
            //for ($i = 1; $i < 10; $i++) {
            //    $fakeCollection[] = [
            //        "title" => "Item title #{$i}",
            //        "subtitle" => "Subtitle description content message more #{$i}",
            //    ];
            //}

            // Items fetch
            $items = (new ModuleSkeleton_Model_Item())->findAll(
                [
                    "value_id = ?" => $optionValue->getId()
                ],
                "item_id ASC"
            );

            $realCollection = [];
            foreach ($items as $item) {
                $realCollection[] = [
                    "id" => (integer) $item->getId(),
                    "title" => (string) $item->getTitle(),
                    "subtitle" => (string) $item->getSubtitle(),
                    "cover" => (string) $item->getCover(),
                ];
            }

            $payload = [
                "success" => true,
                "pageTitle" => (string)$optionValue->getTabbarName(),
                "title" => (string)$skeleton->getTitle(),
                "subtitle" => (string)$skeleton->getSubtitle(),
                "col_textarea" => (string)$skeleton->getColTextarea(),
                "col_select" => (string)$skeleton->getColSelect(),
                "image" => (string)$skeleton->getImage(),
                "skeleton_test" => (string) __get("skeleton_test"),
                "collection" => $realCollection,
                //"collection" => $fakeCollection,
                //"collection" => [],
            ];

        } catch (Exception $e) {
            $payload = [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }

        $this->_sendJson($payload);
    }

    /**
     *
     */
    public function findItemAction()
    {
        try {
            $request = $this->getRequest();
            $itemId = $request->getParam("itemId");

            $item = (new ModuleSkeleton_Model_Item())->find($itemId);

            if (!$item->getId()) {
                throw new Exception(__("This item do not exists."));
            }

            $payload = [
                "success" => true,
                "item" => [
                    "id" => (integer) $item->getId(),
                    "title" => (string) $item->getTitle(),
                    "subtitle" => (string) $item->getSubtitle(),
                    "cover" => (string) $item->getCover(),
                ]
            ];

        } catch (Exception $e) {
            $payload = [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }

        $this->_sendJson($payload);
    }
}
