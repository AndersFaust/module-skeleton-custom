<?php

/**
 * Class ModuleSkeleton_Model_Db_Table_Item
 */
class ModuleSkeleton_Model_Db_Table_Item extends Core_Model_Db_Table
{

    /**
     * @var string
     */
    protected $_name = "skeleton_item";

    /**
     * @var string
     */
    protected $_primary = "item_id";

}