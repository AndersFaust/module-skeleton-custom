<?php

/**
 * Class ModuleSkeleton_Model_Item
 */
class ModuleSkeleton_Model_Item extends Core_Model_Default
{
    /**
     * ModuleSkeleton_Model_Item constructor.
     * @param array $params
     */
    public function __construct($params = [])
    {
        parent::__construct($params);
        $this->_db_table = 'ModuleSkeleton_Model_Db_Table_Item';
        return $this;
    }
}