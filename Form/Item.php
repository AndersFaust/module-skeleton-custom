<?php

/**
 * Class ModuleSkeleton_Form_Item
 */
class ModuleSkeleton_Form_Item extends Siberian_Form_Abstract
{
    /**
     * @throws Zend_Form_Exception
     */
    public function init()
    {
        parent::init();

        $this
            ->setAction(__path("/moduleskeleton/application/save-item"))
            ->setAttrib("id", "form-item");

        self::addClass("create", $this);

        $this->addSimpleHidden("item_id");

        $title = $this->addSimpleText("title", __("Title"));
        $title->setRequired(true);


        $subtitle = $this->addSimpleText("subtitle", __("Subtitle"));
        $subtitle->setRequired(true);

        // Image
        $this->addSimpleImage("cover", __("Cover"), __("Cover"), [
            "width" => 600,
            "height" => 300,
        ]);

        $this->addSimpleHidden("value_id");

        $this->addSubmit(__("Save"))
            ->addClass("default_button")
            ->addClass("pull-right")
            ->addClass("submit_button");

        // changing form
    }

    /**
     * @param $itemId
     */
    public function setItemId($itemId)
    {
        $this
            ->getElement("item_id")
            ->setValue($itemId)
            ->setRequired(true);
    }
}