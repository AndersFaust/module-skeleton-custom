<?php

/**
 * Class ModuleSkeleton_Form_Skeleton
 */
class ModuleSkeleton_Form_Skeleton extends Siberian_Form_Abstract
{
    /**
     * @throws Zend_Form_Exception
     */
    public function init()
    {
        parent::init();

        $this
            ->setAction(__path("/moduleskeleton/application/save"))
            ->setAttrib("id", "form-skeleton");

        self::addClass("create", $this);

        $title = $this->addSimpleText("title", __("Title"));
        $title->setRequired(true);


        $subtitle = $this->addSimpleText("subtitle", __("Subtitle"));
        $subtitle->setRequired(true);


        // Textarea
        $textarea = $this->addSimpleTextarea("col_textarea", __("Textarea input"));
        $textarea->setRichtext();

        // Select
        $this->addSimpleSelect("col_select", __("Select input"), [
            "none" => __("None"),
            "all" => __("All"),
        ]);

        // Image
        $this->addSimpleImage("image", __("Image"), __("Image"), [
            "width" => 512,
            "height" => 512,
        ]);

        $this->addSimpleHidden("value_id");

        $this->addSubmit(__("Save"))
            ->addClass("default_button")
            ->addClass("pull-right")
            ->addClass("submit_button");

        // changing form
    }
}