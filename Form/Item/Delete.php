<?php

/**
 * Class ModuleSkeleton_Form_Item_Delete
 */
class ModuleSkeleton_Form_Item_Delete extends Siberian_Form_Abstract
{
    /**
     * @throws Zend_Form_Exception
     */
    public function init()
    {
        parent::init();

        $this
            ->setAction(__path("/moduleskeleton/application/item-delete"))
            ->setAttrib("id", "form-item-delete")
            ->setConfirmText("You are about to remove this Item! Are you sure ?");

        /** Bind as a delete form */
        self::addClass("delete", $this);

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from('skeleton_item')
            ->where('skeleton_item.item_id = :value');

        $itemId = $this->addSimpleHidden('item_id', __('Item'));
        $itemId->addValidator('Db_RecordExists', true, $select);
        $itemId->setMinimalDecorator();

        $valueId = $this->addSimpleHidden('value_id');
        $valueId
            ->setRequired(true);

        $miniSubmit = $this->addMiniSubmit();
    }
}