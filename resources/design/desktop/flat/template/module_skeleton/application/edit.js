/*global
 bindForms
 */
$(document).ready(function () {
    bindForms('#items');
    bindForms('#skeleton');
    bindForms('#design');
});
