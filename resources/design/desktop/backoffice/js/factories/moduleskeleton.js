/**
 *
 */
App.factory("Skeleton", function ($http, Url) {
    var factory = {};

    /**
     *
     * @returns {*}
     */
    factory.loadData = function () {
        return $http({
            method: "GET",
            url: Url.get("moduleskeleton/backoffice_view/load"),
            cache: false
        });
    };

    /**
     *
     * @param settings
     * @returns {*}
     */
    factory.save = function (settings) {
        return $http({
            method: "POST",
            url: Url.get("moduleskeleton/backoffice_view/save"),
            data: settings,
            cache: false
        });
    };
	
    return factory;
});
