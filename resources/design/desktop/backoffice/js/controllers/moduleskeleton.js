/**
 * Module Skeleton backoffice settings
 */
App.config(function ($routeProvider) {
    $routeProvider
        .when(BASE_URL + '/moduleskeleton/backoffice_view', {
            controller: 'SkeletonViewController',
            templateUrl: BASE_URL + '/moduleskeleton/backoffice_view/template'
        });
}).controller('SkeletonViewController', function ($scope, $window, Header, Skeleton) {
    angular.extend($scope, {
        header: new Header(),
        content_loader_is_visible: true,
        settings: {}
    });

    $scope.header.loader_is_visible = false;

    Skeleton
        .loadData()
        .success(function (payload) {
            $scope.header.title = payload.title;
            $scope.header.icon = payload.icon;
            $scope.settings = payload.settings;
        })
        .finally(function () {
            $scope.content_loader_is_visible = false;
        });

    $scope.save = function () {
        $scope.content_loader_is_visible = true;
        Skeleton
            .save($scope.settings)
            .success(function (payload) {
                $scope.message
                    .setText(payload.message)
                    .isError(false)
                    .show();
            }).error(function (payload) {
                $scope.message
                    .setText(payload.message)
                    .isError(true)
                    .show();
            }).finally(function () {
                $scope.content_loader_is_visible = false;
            });
    };

});
