/**
 * Skeleton
 *
 * @version 1.0.0
 */
angular.module("starter").controller("SkeletonIndexController", function ($scope, $stateParams, $state, $translate,
                                                                          $timeout, Dialog, Skeleton) {
    angular.extend($scope, {
        isLoading: true,
        value_id: $stateParams.value_id,
        pageTitle: $translate.instant("Skeleton", "skeleton"),
        skeleton: {
            title: "",
            subtitle: "",
            col_textarea: "",
            col_select: "",
            image: ""
        },
        collection: []
    });

    Skeleton.setValueId($stateParams.value_id);

    /**
     * Load page payload
     */
    $scope.loadContent = function (refresh) {
        Skeleton
        .loadContent(refresh)
        .then(function (payload) {
            $timeout(function () {
                $scope.collection = payload.collection;
                $scope.pageTitle = payload.pageTitle;
                $scope.skeleton.title = payload.title;
                $scope.skeleton.subtitle = payload.subtitle;
                $scope.skeleton.col_textarea = payload.col_textarea;
                $scope.skeleton.col_select = payload.col_select;
                $scope.skeleton.image = payload.image;
            });
        }, function (error) {
            Dialog.alert("Error", error.message, "OK", 3500, "skeleton");
        });
    };

    $scope.imagePath = function (image) {
        return IMAGE_URL + "/images/application" + image;
    };

    $scope.refresh = function () {
        $scope.loadContent(true);
    };

    $scope.showItem = function (item) {
        $state.go("skeleton-view", {
            value_id: $stateParams.value_id,
            item_id: item.id
        });
    };

    $scope.loadContent(false);
});


angular.module("starter").controller("SkeletonViewController", function ($scope, $stateParams, $timeout, $pwaRequest, Dialog, Skeleton) {
    angular.extend($scope, {
        isLoading: true,
        value_id: $stateParams.value_id,
        itemId: $stateParams.item_id,
        item: null
    });

    /**
     * Load page payload
     */
    $scope.findItem = function (itemId) {
        Skeleton
            .findItem(itemId)
            .then(function (payload) {
                // I have my item, but still not my image
                $pwaRequest
                .get($scope.imagePath(payload.item.image))
                .then(function () {
                    $timeout(function () {
                        $scope.item = payload.item;
                    });
                });

            }, function (error) {
                Dialog.alert("Error", error.message, "OK", 3500, "skeleton");
            }).then(function () {
                $scope.isLoading = false;
            });
    };

    $scope.imagePath = function (image) {
        if (image.length <= 0) {
            return IMAGE_URL + "/app/local/modules/ModuleSkeleton/features/module_skeleton/assets/images/big-image.bmp";
        }
        return IMAGE_URL + "/images/application" + image;
    };

    $scope.findItem($scope.itemId);
});
